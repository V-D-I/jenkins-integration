DROP TABLE IF EXISTS client;
create table client
(
    id   SERIAL PRIMARY KEY,
    name varchar(40) NOT NULL
)