package com.epam.jenkins_integration.repository;

import com.epam.jenkins_integration.entity.ClientEntity;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("test")
@SpringBootTest
public class ClientRepositoryTest {

    @Autowired
    private ClientRepository clientRepository;


    @Test
    public void testClientCreation() {
        System.out.println("testClientCreation");
//        ClientEntity clientEntity = new ClientEntity();
//        clientEntity.setName("Bob");
//
//        clientRepository.save(clientEntity);
//
//        assertNotEquals(0L, clientEntity.getId());
        assertTrue(true);
    }

    @Test
    @Disabled("This test disabled for fun")
    public void failedTest() {
        System.out.println("failedTest");
        fail();
    }

    @Test
    public void successTest() {
        System.out.println("successTest");
        assertTrue(true);
    }
}