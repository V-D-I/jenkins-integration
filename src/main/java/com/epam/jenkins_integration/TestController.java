package com.epam.jenkins_integration;

import com.epam.jenkins_integration.entity.ClientEntity;
import com.epam.jenkins_integration.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @Autowired
    private ClientRepository clientRepository;

    @GetMapping(value = "/test")
    public String test() {
        ClientEntity clientEntity = new ClientEntity();
        clientEntity.setName("Bob");
        clientRepository.save(clientEntity);
        return clientEntity.toString();
    }
}
